const isValid = require("./validator");
const { barSchema, barObj, barObjF } = require("../examples/bars");
const { personSchema, personObj, personObjF } = require("../examples/persons");
const { carSchema, carObj, carObjF } = require("../examples/cars");

const objects = [
  [barSchema, barObj, true],
  [barSchema, barObjF, false],
  [personSchema, personObj, true],
  [personSchema, personObjF, false],
  [carSchema, carObj, true],
  [carSchema, carObjF, false],
];

it.each(objects)(
  "should test the correct outcome of all types of calculations",
  (schema, object, returnvalue) => {
    const result = isValid(schema, object);
    expect(result).toBe(returnvalue);
  }
);
