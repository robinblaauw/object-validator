const isValid = (schema, obj) => {
  let invalidEntries = [];
  const schemaEntries = Object.keys(schema);

  const typeCheck = (entry, entryType, key) => {
    switch (typeof entry) {
      // Check if type is Array while schema entry is object
      case "object":
        if (entry instanceof Array && entryType === "object") {
          invalidEntries.push(key);
          break;
        } else {
          return entry;
        }
      default:
        return typeof entry === entryType ? entry : invalidEntries.push(key);
    }
  };

  for (const key of schemaEntries) {
    // If the provided object does not contain key that the schema has add it to invalidEntires array
    obj[key] === undefined
      ? invalidEntries.push(key)
      : typeCheck(obj[key], schema[key], key);
  }
  // If there are invalid entries the function returns false
  return invalidEntries.length === 0 ? true : false;
};

module.exports = isValid;
