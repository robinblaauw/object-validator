exports.personSchema = {
  name: "string",
  age: "number",
  siblings: "array",
  metaData: "object",
  active: "boolean",
};

// Validates true
exports.personObj = {
  name: "James",
  age: 25,
  siblings: ["Johnnathan"],
  metaData: {},
  active: true,
};

// Validates false
exports.personObjF = {
  name: "James",
  age: 25,
  active: true,
};
